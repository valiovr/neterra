<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/',function(){
    return view('welcome');
});
//
Route::get('/home', 'UserController@index');
Route::get('/news/add',function(){
    return view('news.add');
});
Route::post('/news/add','NewsController@postNews');
Route::get('/news/approve','UserController@admin');
Route::post('/news/approve','NewsController@postApproveNews');

Route::get('/news/json','NewsController@newsToJson');
Route::get('/news/xml','NewsController@newsToXml');

// Authentication routes...
Route::get('auth/login', 'Auth\AuthController@getLogin');
Route::post('auth/login', 'Auth\AuthController@postLogin');
Route::get('auth/logout', 'Auth\AuthController@getLogout');

// Registration routes...
Route::get('auth/register', 'Auth\AuthController@getRegister');
Route::post('auth/register', 'Auth\AuthController@postRegister');
