<?php

namespace App\Http\Controllers;

use App\News;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Support\Facades\Auth;
use Session;
use Response;
use App\Http\Controllers\NewsController;
use Illuminate\Support\Facades\Mail;

class UserController extends Controller
{
    public function index()
    {
        $user = new User();

        if (Auth::user()) {
            $news = new NewsController();
            $data['is_admin'] = $user->is_admin();
            $data['news'] = $news->getNews();

            return view('user.home', $data);
        }
        header("Location: " . url() . " ");
        die();
    }

    public function admin()
    {
        $user = new User();

        if ($user->is_admin()) {
            $data = array();
            $news = new NewsController();
            $unApprovedNews = $news->getUnapprovedNews();
            $data['unapproved'] = $unApprovedNews;
            return view('admin.approve', $data);
        }
        header("Location: " . url() . " ");
        die();
    }

    public function notifyAdmin()
    {
        $admin = User::where('is_admin', 1)->get()->first()->toArray();
         Mail::send('emails.admin', ['name' => $admin['name']], function ($message) use ($admin) {

            $message->to($admin['email'])->subject('Pending news');
        });
        return true;
    }

    public function notifyUser($news_id)
    {

        $user = News::join('users', 'users.id', '=', 'news.author')
        ->select('users.id','users.name','users.email','news.title')->where('news.id', $news_id)->get()->first()->toArray();

        Mail::send('emails.user', ['name' => $user['name'],'title'=>$user['title']], function($message) use ($user) {

            $message->to($user['email'])->subject('Your news was approved!');
        });
        return true;
    }

}
