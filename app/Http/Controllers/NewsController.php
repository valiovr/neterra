<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\News;
use Illuminate\Support\Facades\Input;
use Image;
class NewsController extends Controller
{
    public function getNews()
    {
        $news = News::get()
            ->toArray();
        return $news;
    }

    public function addNews()
    {
        echo 'in add news';
        die;
    }

    public function postNews(Requests\StoreNewsRequest $request)
    {
        $news = new News();
        $news->title = $request->title;
        $news->author = $request->author;
        $news->text = $request->text;

        if(Input::file())
        {
            $image = Input::file('image');
            $filename  = time() . '.' . $image->getClientOriginalExtension();
//            $path = public_path('pictures\\' . $filename);
            $path = base_path().'/public/pictures/upload/' . $filename;

            Image::make($image->getRealPath())->resize(200, 200)->save($path);
            $news->image = $filename;
        }

        $news->save();
        if ($news) {
        $user = new UserController();
            $user->notifyAdmin();
            return redirect(url() . '/home');
        }
    }

    public function getUnapprovedNews()
    {
        return News::where('approved','0')->get()->toArray();
    }

    public function postApproveNews(Request $request)
    {

        $news = new News();
        $update = $news->where('id',$request->approve)->update(['approved' => 1]);
        if($update) {
            $user = new UserController();
            $user->notifyUser($request->approve);
            return redirect(url() . '/news/approve');
        }
    }

    public function newsToJson()
    {
        $news = News::get()->toArray();
        echo json_encode($news);die;
    }

    public function newsToXml()
    {
        $news = News::all();
        $xml = new \XMLWriter();

        $xml->openMemory();
        $xml->startDocument();
        $xml->startElement('users');
        foreach($news as $xm) {
            $xml->startElement('data');
            $xml->writeAttribute('id', $xm->id);
            $xml->writeAttribute('title', $xm->title);
            $xml->writeAttribute('text', $xm->text);
            $xml->writeAttribute('image', $xm->image);
            $xml->writeAttribute('author', $xm->author);
            $xml->writeAttribute('approved', $xm->approved);
            $xml->writeAttribute('created_at', $xm->created_at);
            $xml->writeAttribute('updated_at', $xm->updated_at);
            $xml->endElement();
        }
        $xml->endElement();
        $xml->endDocument();

        $content = $xml->outputMemory();
        $xml = null;

        return response($content)->header('Content-Type', 'text/xml');
    }

}
