<!DOCTYPE html>
<html>
<head>
    @include('main')
    <title>Home</title>
    {{--<meta name="csrf-token" content="{{ csrf_token() }}"/>--}}
</head>
<body>
<nav class="navbar navbar-default navbar-static-top">
    <div class="container">
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-8">
            <ul class="nav navbar-nav">
                <li><a href="{{url()}}/home">Home</a></li>
                <li class="active"><a href="{{url()}}/news/add">Post news</a></li>

                @if(Auth::user()->is_admin == 1)
                    <li><a href="{{url()}}/auth/logout">Approve news</a></li>
                @endif

                <li><a href="{{url()}}/auth/logout">Logout</a></li>
            </ul>
        </div>

    </div>
</nav>
    <div style="max-width:700px;" class="well center-block">

        <form enctype="multipart/form-data" accept-charset="UTF-8" method="post" action="{{ url() }}/news/add">
            {{--{!! Form::open(array('url'=>'/news/add','method'=>'POST', 'files'=>true)) !!}--}}
            {{--{!! csrf_field() !!}--}}
            @foreach($errors->all() as $error)
                <div class="alert alert-danger" role="alert"> {!! $error !!} </div>
            @endforeach
            <div class="form-group">
                <label>Title</label>
                <input type="text" class="form-control" name="title"/>

            </div>
            <div class="form-group">
                <label>Text</label>
                <textarea class="form-control" name="text" id="text_area" cols="30" rows="10"></textarea>
            </div>
            <div class="form-group">
                <input type="hidden" name="author" value="{{ Auth::user()->id }}">
            </div>
            <div class="form-group">
                <input type="file" name="image">
            </div>
            <div id="error_msg" class="alert alert-danger hidden" role="alert"></div>
            <button class="btn btn-default btn-lg btn-block" type="submit">Add</button>


        </form>

    </div>

</body>
</html>