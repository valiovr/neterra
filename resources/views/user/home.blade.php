<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/html">
<head>
    @include('main')
    <title>Home</title>
    <meta name="csrf-token" content="{{ csrf_token() }}"/>
</head>
<body>
<nav class="navbar navbar-default navbar-static-top">
    <div class="navbar-header">
        <a class="navbar-brand">Welcome {{ Auth::user()->name }} !</a>
    </div>
    <div class="container">
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-8">
            <ul class="nav navbar-nav">
                <li class="active"><a href="{{url()}}/home">Home</a></li>
                <li><a href="{{url()}}/news/add">Post news</a></li>
                @if($is_admin == 1)
                    <li><a href="{{url()}}/news/approve">Approve news</a></li>
                @endif

                <li><a href="{{url()}}/auth/logout">Logout</a></li>
            </ul>
        </div>
    </div>
</nav>

<div class="container">
    <a target="_blank" href="{{ url()}}/news/json" ><button class="btn btn-large">Export all news to JSON</button></a>
    <a target="_blank" href="{{ url() }}/news/xml" ><button class="btn btn-large">Export all news to XML</button></a>
    <h3>Approved News</h3>
    <table class="table table-striped">
        <thead>
        <th>ID</th>
        <th>Title</th>
        <th>Text</th>
        <th>Created</th>
        <th>Image</th>
        {{--<th>Approved</th>--}}
        </thead>
        @foreach($news as $new)
            @if($new['approved'] == 1)
                <tr>
                    <td>{{ $new['id'] }}</td>
                    <td>{{ $new['title'] }}</td>
                    <td>{{ $new['text'] }}</td>
                    <td>{{ $new['created_at'] }}</td>
                    @if($new['image'])
                        <td><img src="{{ url().'/pictures/upload/'. $new['image'] }}" width="100"/></td>
                    @else
                        <td><img src="{{ url().'/pictures/upload/no-image.png' }}" width="100"/></td>
                    @endif
                </tr>
            @endif
        @endforeach
    </table>
</div>
</body>
</html>