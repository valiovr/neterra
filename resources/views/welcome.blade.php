<!DOCTYPE html>
<html>
<head>
    @include('main')
    <title>Home</title>
    <meta name="csrf-token" content="{{ csrf_token() }}" />
</head>
<body>
<nav class="navbar navbar-default navbar-static-top">
    <div class="container">
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-8">
            <ul class="nav navbar-nav">
                <li class="active"><a href="{{url()}}/home">Home</a></li>
                <li><a href="{{url()}}/auth/register">Register</a></li>
                <li><a href="{{url()}}/auth/login">Login</a></li>
            </ul>
        </div>
    </div>
</nav>
</body>
</html>