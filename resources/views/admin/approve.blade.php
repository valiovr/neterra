<!DOCTYPE html>
<html>
<head>
    @include('main')
    <title>Approve news</title>
</head>
<body>
<nav class="navbar navbar-default navbar-static-top">
    <div class="container">
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-8">
            <ul class="nav navbar-nav">
                <li><a href="{{url()}}/home">Home</a></li>
                <li><a href="{{url()}}/news/add">Post news</a></li>
                <li class="active"><a href="{{url()}}/auth/logout">Approve news</a></li>
                <li><a href="{{url()}}/auth/logout">Logout</a></li>
            </ul>
        </div>
    </div>
</nav>

<div class="container">
    <form method="post" action="{{ url() }}/news/approve">
    <table class="table table-striped">
        <thead>
        <th>ID</th>
        <th>Title</th>
        <th>Text</th>
        <th>Created</th>
        <th>Approve </th>
        </thead>
        @foreach($unapproved as $news)
                <tr>
                    <td>{{ $news['id'] }}</td>
                    <td>{{ $news['title'] }}</td>
                    <td>{{ $news['text'] }}</td>
                    <td>{{ $news['created_at'] }}</td>
                    <td><button type="submit" name="approve" value="{{ $news['id'] }}">Approve</button></td>
                </tr>
        @endforeach

    </table>
</form>
</div>



</body>
</html>