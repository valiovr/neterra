<!DOCTYPE html>
<html>
<head>
    @include('main')
    <title>Register</title>
</head>
<body>
<nav class="navbar navbar-default navbar-static-top">
    <div class="container">
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-8">
            <ul class="nav navbar-nav">
                <li><a href="{{url()}}/home">Home</a></li>
                <li class="active"><a href="{{url()}}/auth/register">Register</a></li>
                <li><a href="{{url()}}/auth/login">Login</a></li>
            </ul>
        </div>
    </div>
</nav>
<div style="max-width:400px;" class="well center-block">

    <form id="register_form" method="POST" action="../auth/register">
        {!! csrf_field() !!}
        <div class="form-group">
            <label>Name</label>
            <input type="text" class="form-control" name="name"/>
        </div>
        <div class="form-group">
            <label>E-mail</label>
            <input id="user_email" type="text" class="form-control" name="email"/>
        </div>
        <div class="form-group">
            <label>Password</label>
            <input id="pass" type="password" class="form-control" name="password"/>
            <label>Repeat Password</label>
            <input id="pass2" type="password" class="form-control" name="password_confirmation"/>
        </div>
        <div class="form-group">
            <label>admin</label>
            <input type="hidden" name="is_admin" value="0" />
            {!! Form::checkbox('is_admin', '1') !!}
        </div>
        <div id="error_msg" class="alert alert-danger hidden" role="alert"></div>
        <button class="btn btn-default btn-lg btn-block" type="submit">Register</button>

        @foreach($errors->all() as $error)
            <div class="alert alert-danger" role="alert"> {!! $error !!} </div>
        @endforeach
    </form>

</div>
</body>
</html>