
<html>
<head>
    @include('main')
    <title>Login</title>
    <meta name="csrf-token" content="{{ csrf_token() }}" />
</head>
<body>
<nav class="navbar navbar-default navbar-static-top">
    <div class="container">
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-8">
            <ul class="nav navbar-nav">
                <li><a href="{{url()}}/home">Home</a></li>
                <li><a  href="{{url()}}/auth/register">Register</a></li>
                <li class="active"><a href="{{url()}}/auth/login">Login</a></li>
            </ul>
        </div>
    </div>
</nav>
<div style="max-width:400px;" class="well center-block">

    <form method="post" action="../auth/login">

        {!! csrf_field() !!}
        <div class="form-group">
            <label>Email</label>
            <input type="text" class="form-control" name="email" />

        </div>
        <div class="form-group">
            <label>Password</label>
            <input type="password" class="form-control" name="password" />
        </div>
        <div id="error_msg" class="alert alert-danger hidden" role="alert"></div>
        <button id="login_button" class="btn btn-default btn-lg btn-block" type="submit">Login</button>
       <input type="hidden" name="_token" value="{{ csrf_token() }}">

        @foreach($errors->all() as $error)
            <div class="alert alert-danger" role="alert"> {!! $error !!} </div>
        @endforeach
    </form>

</div>
</body>


</html>